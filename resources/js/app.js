import Vue from "vue";
import VueRouter from "vue-router";
import Home from "./components/Home.vue";
import About from "./components/About.vue";
import headers from "./components/Header.vue";
import footers from "./components/Footer.vue";
import modals from "./components/Modal.vue";
import showmodals from './components/ShowModal.vue';
Vue.use(VueRouter);
require('./bootstrap');

// Vue.component("headers", require("./components/Header.vue").default);
// Vue.component("footers", require("./components/Footer.vue").default);

const routes = [{ path: "/home", component: Home }, { path: "/about", component: About }];
const router = new VueRouter({
	routes
	// mode: 'history'
});
const app = new Vue({
	el: "#app",
	router,
	components: { headers, footers, modals, showmodals }
});
