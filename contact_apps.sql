-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 14, 2019 at 10:20 PM
-- Server version: 8.0.17
-- PHP Version: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `contactapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact_apps`
--

CREATE TABLE `contact_apps` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_apps`
--

INSERT INTO `contact_apps` (`id`, `name`, `phone`, `email`, `created_at`, `updated_at`) VALUES
(38, 'Abhiyan Shrestha', '9841706508', 'abhiyaan@yahoo.com', '2019-09-06 07:33:29', '2019-09-06 07:33:47'),
(39, 'Ajay', '776213123', 'ajay@gmail.com', '2019-09-06 07:36:14', '2019-09-06 07:36:14'),
(40, 'zxczxc', '414524242', 'asd@yag.com', '2019-09-06 07:39:58', '2019-09-06 07:39:58'),
(41, 'asd', '123123', 'abhiy123aan@yahoo.com', '2019-09-06 07:41:33', '2019-09-06 07:41:33'),
(42, 'ascfzacf', '243234234', 'abhiyaan@yaqashoo.com', '2019-09-06 08:59:53', '2019-09-06 08:59:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact_apps`
--
ALTER TABLE `contact_apps`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact_apps`
--
ALTER TABLE `contact_apps`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
