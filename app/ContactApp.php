<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactApp extends Model
{
    protected $table = 'contact_apps';
    protected $hidden = ['created_at', 'updated_at'];
}
